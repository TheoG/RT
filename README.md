# RT

Deuxième version et dernier projet de Ray Tracer de l'école 42.

(Voir la première version ici : https://gitlab.com/TheoG/RTv1)

Contrairement au projet précédent, ce dernier a été par un groupe de 4 étudiants.

### Concepts ajoutés
- Rendu avec le GPU (Cuda)
- Rendu en temps réel
- Ajout d'une interface graphique GTK
- Amélioration de la transparence avec les effets Fresnel et Beer-Lambert
- Illumination Globale
- Lumières parallèles
- Ajouts de textures
-- Chargées au format BMP
-- Générées : Bruit Monochromatique, Bruit de Perlin, Échiquier
- Normal Mapping
- Antialiasing (x2 à x4)
- Filtres (Noir et Blanc, Sepia et Daltonien Vert-Rouge, Cartoon, Anaglyphe)
- Prise en charge complète de notre format RT (Open, Save, Save As, ...)
- Photon Mapping et caustiques


Vidéo de présentation des fonctionalitées : https://youtu.be/m4NBhRF1hxs